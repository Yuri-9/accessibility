const TIME_UPDATE_MESSAGE = 60000;

(function () {
  var burger = document.querySelector(".burger");
  var menu = document.querySelector("#" + burger.dataset.target);
  burger.addEventListener("click", function () {
    burger.classList.toggle("is-active");
    burger.setAttribute(
      "aria-expanded",
      burger.getAttribute("aria-expanded") === "true" ? "false" : "true"
    );
    menu.classList.toggle("is-active");
  });
})();

let numberMassage = 0;
const numberNewMassageEl = document.querySelector("#number-new-massage");

setInterval(() => {
  numberNewMassageEl.innerText = `There are ${++numberMassage} new messages in the forum`;
}, TIME_UPDATE_MESSAGE);

class TabsList {
  constructor(tabListNode) {
    this.tabListNode = tabListNode;

    this.tabs = [];
    this.tabs = Array.from(this.tabListNode.querySelectorAll("[role=tab]"));

    this.firstTab = this.tabs[0];
    this.lastTab = this.tabs[this.tabs.length - 1];

    const tabPanels = Array.from(document.querySelectorAll(".tab-pane"));
    this.tabPanelsMap = tabPanels?.reduce(
      (acc, panel) => acc.set(panel.id, panel),
      new Map()
    );

    this.tabs.forEach((tab) => {
      tab.addEventListener("keydown", (e) => this.onKeydown(e));
      tab.addEventListener("click", (e) =>
        this.setSelectedTab(e.currentTarget)
      );
    });

    this.setSelectedTab(this.firstTab, false);
  }

  setSelectedTab(currentTab, setFocus = true) {
    this.tabs.forEach((tab) => {
      const panelId = tab.dataset.target;

      if (currentTab === tab) {
        tab.classList.add("is-active");
        tab.setAttribute("aria-selected", "true");
        tab.setAttribute("tabindex", "0");

        this.tabPanelsMap.get(panelId).style.display = "block";

        setFocus && tab.focus();
      } else {
        tab.classList.remove("is-active");
        tab.setAttribute("aria-selected", "false");
        tab.setAttribute("tabindex", "-1");

        this.tabPanelsMap.get(panelId).style.display = "none";
      }
    });
  }

  setSelectedToPreviousTab(currentTab) {
    if (currentTab === this.firstTab) {
      this.setSelectedTab(this.lastTab);
    } else {
      const index = this.tabs.indexOf(currentTab);
      this.setSelectedTab(this.tabs[index - 1]);
    }
  }

  setSelectedToNextTab(currentTab) {
    if (currentTab === this.lastTab) {
      this.setSelectedTab(this.firstTab);
    } else {
      const index = this.tabs.indexOf(currentTab);
      this.setSelectedTab(this.tabs[index + 1]);
    }
  }

  onKeydown(e) {
    const currentTab = e.currentTarget;
    let flag = false;

    switch (e.key) {
      case "ArrowLeft":
        this.setSelectedToPreviousTab(currentTab);
        flag = true;
        break;

      case "ArrowRight":
        this.setSelectedToNextTab(currentTab);
        flag = true;
        break;

      case "Home":
        this.setSelectedTab(this.firstTab);
        flag = true;
        break;

      case "End":
        this.setSelectedTab(this.lastTab);
        flag = true;
        break;

      default:
        break;
    }

    if (flag) {
      e.stopPropagation();
      e.preventDefault();
    }
  }
}

class MenuButtonLinks {
  constructor(domNode) {
    this.domNode = domNode;

    this.menuNode = this.domNode.querySelector('[role="menu"]');
    this.menuNode.addEventListener("focusin", (e) => this.onFocusin(e));
    this.menuNode.addEventListener("focusout", (e) => this.onFocusout(e));

    this.buttonNode = this.domNode.querySelector("button");
    this.buttonNode.addEventListener("keydown", (e) => this.onButtonKeydown(e));
    this.buttonNode.addEventListener("click", (e) => this.onButtonClick(e));

    this.menuitemNodes = Array.from(this.menuNode.querySelectorAll("a"));
    this.menuitemNodes.forEach((menuitem) => {
      menuitem.addEventListener("keydown", (e) => this.onMenuitemKeydown(e));
      menuitem.addEventListener("mouseover", (e) =>
        this.onMenuitemMouseover(e)
      );
    });

    this.firstMenuitem = this.menuitemNodes[0];
    this.lastMenuitem = this.menuitemNodes[this.menuitemNodes.length - 1];

    window.addEventListener(
      "mousedown",
      (e) => this.onBackgroundMousedown(e),
      true
    );
  }

  setFocusToMenuitem(newMenuitem) {
    this.menuitemNodes.forEach((item) => {
      if (item === newMenuitem) {
        item.tabIndex = 0;
        newMenuitem.focus();
      } else {
        item.tabIndex = -1;
      }
    });
  }

  setFocusToFirstMenuItem() {
    this.setFocusToMenuitem(this.firstMenuitem);
  }

  setFocusToLastMenuItem() {
    this.setFocusToMenuitem(this.lastMenuitem);
  }

  setFocusToPreviousMenuItem(currentMenuitem) {
    let newMenuitem = null;
    let index;

    if (currentMenuitem === this.firstMenuitem) {
      newMenuitem = this.lastMenuitem;
    } else {
      index = this.menuitemNodes.indexOf(currentMenuitem);
      newMenuitem = this.menuitemNodes[index - 1];
    }

    this.setFocusToMenuitem(newMenuitem);

    return newMenuitem;
  }

  setFocusToNextMenuItem(currentMenuitem) {
    var newMenuitem, index;

    if (currentMenuitem === this.lastMenuitem) {
      newMenuitem = this.firstMenuitem;
    } else {
      index = this.menuitemNodes.indexOf(currentMenuitem);
      newMenuitem = this.menuitemNodes[index + 1];
    }
    this.setFocusToMenuitem(newMenuitem);

    return newMenuitem;
  }

  // Popup menu methods

  openPopup() {
    this.menuNode.style.display = "block";
    this.buttonNode.setAttribute("aria-expanded", "true");
  }

  closePopup() {
    if (this.isOpen()) {
      this.buttonNode.setAttribute("aria-expanded", "false");
      this.menuNode.style.display = "none";
    }
  }

  isOpen() {
    return this.buttonNode.getAttribute("aria-expanded") === "true";
  }

  // Menu event handlers

  onFocusin() {
    this.menuNode.classList.add("focus");
  }

  onFocusout() {
    this.menuNode.classList.remove("focus");
  }

  onButtonKeydown(event) {
    var key = event.key,
      flag = false;

    switch (key) {
      case " ":
      case "Enter":
      case "ArrowDown":
      case "Down":
        this.openPopup();
        this.setFocusToFirstMenuItem();
        flag = true;
        break;

      case "Esc":
      case "Escape":
        this.closePopup();
        this.buttonNode.focus();
        flag = true;
        break;

      case "Up":
      case "ArrowUp":
        this.openPopup();
        this.setFocusToLastMenuItem();
        flag = true;
        break;

      default:
        break;
    }

    if (flag) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  onButtonClick(event) {
    if (this.isOpen()) {
      this.closePopup();
      this.buttonNode.focus();
    } else {
      this.openPopup();
      this.setFocusToFirstMenuItem();
    }

    event.stopPropagation();
    event.preventDefault();
  }

  onMenuitemKeydown(event) {
    var tgt = event.currentTarget,
      key = event.key,
      flag = false;

    if (event.ctrlKey || event.altKey || event.metaKey) {
      return;
    }

    if (event.shiftKey) {
      if (event.key === "Tab") {
        this.buttonNode.focus();
        this.closePopup();
        flag = true;
      }
    } else {
      switch (key) {
        case " ":
          window.location.href = tgt.href;
          break;
        case "Enter":
          this.buttonNode.blur();
          this.closePopup();
          break;

        case "Esc":
        case "Escape":
          this.closePopup();
          this.buttonNode.focus();
          flag = true;
          break;

        case "Up":
        case "ArrowUp":
          this.setFocusToPreviousMenuItem(tgt);
          flag = true;
          break;

        case "ArrowDown":
        case "Down":
          this.setFocusToNextMenuItem(tgt);
          flag = true;
          break;

        case "Home":
        case "PageUp":
          this.setFocusToFirstMenuItem();
          flag = true;
          break;

        case "End":
        case "PageDown":
          this.setFocusToLastMenuItem();
          flag = true;
          break;

        case "Tab":
          this.closePopup();
          break;

        default:
          break;
      }
    }

    if (flag) {
      event.stopPropagation();
      event.preventDefault();
    }
  }

  onMenuitemMouseover(event) {
    var tgt = event.currentTarget;
    tgt.focus();
  }

  onBackgroundMousedown(event) {
    if (!this.menuNode.contains(event.target)) {
      if (this.isOpen()) {
        this.closePopup();
        this.buttonNode.focus();
      }
    }
  }
}

window.addEventListener("load", () => {
  const tabList = document.querySelector(".tablist");
  new TabsList(tabList);

  var menuButton = document.querySelector(".menu-navigate");
  new MenuButtonLinks(menuButton);
});
